# livekit server-sdk-go issue-49

[issue](https://github.com/livekit/server-sdk-go/issues/49)

## how to build and run

start sfu as a single docker container

```bash
cd server
./single-process
```

start sfu with redis using docker-compose

```bash
cd server
docker-compose up
```

join with a `server-sdk-go` client

```bash
cd client

go run client.go token-first
# or 
go run client.go token-second
```

