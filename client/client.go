package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/livekit/protocol/livekit"
	lksdk "github.com/livekit/server-sdk-go"
)

func main() {
	if len(os.Args[1:]) != 1 {
		log.Fatalf("Usage: %s <token-file>", os.Args[0])
	}
	tokenFile := os.Args[1]

	host := "ws://localhost:7880"
	token := readFile(tokenFile)

	room, err := lksdk.ConnectToRoomWithToken(host, token, lksdk.WithAutoSubscribe(false))
	check(err)

	room.LocalParticipant.Callback.OnDataReceived = func(data []byte, rp *lksdk.RemoteParticipant) {
		log.Printf("LocalParticipant.Callback.OnDataReceived from: '%s', SID: '%s', len(data): %d", rp.Identity(), rp.SID(), len(data))
	}
	room.Callback.OnDataReceived = func(data []byte, rp *lksdk.RemoteParticipant) {
		log.Printf("OnDataReceived from: '%s', SID: '%s', len(data): %d", rp.Identity(), rp.SID(), len(data))
	}

	room.Callback.OnParticipantConnected = func(rp *lksdk.RemoteParticipant) {
		log.Printf("OnParticipantConnected from, Identity: '%s', SID: '%s'", rp.Identity(), rp.SID())
		log.Printf("first PublishData")
		err := room.LocalParticipant.PublishData([]byte("123"), livekit.DataPacket_RELIABLE, []string{rp.SID()})
		check(err)
		time.Sleep(time.Second)
		log.Printf("second PublishData")
		err = room.LocalParticipant.PublishData([]byte("123"), livekit.DataPacket_RELIABLE, []string{rp.SID()})
		check(err)
	}

	log.Printf("connected to room '%s'", room.Name)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

	log.Printf("disconnecting from room '%s'", room.Name)
	room.Disconnect()
}

func readFile(file string) string {
	contentData, err := os.ReadFile(file)
	check(err)
	return string(contentData)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
